---
menu: "main"
title: "A propos"
type: "about"
slug: "a-propos"
weight: 10
---

![about](/Adrien-Lasserre.png) 

Je m'appelle Adrien Lasserre, et je suis consultant en cybersécurité sous le nom de marque "Parasélène". Avec plus de 15 ans d'expérience dans le domaine du conseil aux entreprises via le développement web, la supervision d'infrastructures systèmes et de la gestion de projets techniques, je propose désormais mes services aux entreprises et associations pour les aider à renforcer leur sécurité numérique.


## Mon rôle : accompagner les entreprises et associations pour prévenir les cyberattaques.

Je contribue à la conception, à la mise en œuvre et à l'audit de solutions de sécurité de l'information pour des clients variés, en respectant les normes et les bonnes pratiques ISO 27001 et ISO 27005.
Je possède plusieurs certifications reconnues en matière de gestion des risques et d'analyse des risques (eBios RM, ISO27001 Lead Implementer).

## Mes atouts : un profil complet

**Côté pile : une expertise technique** poussée avec plus de 15 ans d'expérience dans le développement d'applications web, gestion de serveurs et administration réseau.

**Côté face : des compétences en gouvernance et conseil en entreprises** avec un diplôme de Sciences Politiques Lyon (IEP) en Relations Internationales, et des certifications en Gestion du risque (ISO 27005 & EBIOS RM) et en Sécurité de l'Information (ISO 27001).

## Mon approche : l'humain d'abord

Mon expérience de développeur et d'administrateur système me permet de comprendre finement les enjeux techniques de la cybersécurité (voir page : ["Expertise technique"]({{< ref "/services/expertise" >}})).

**Néanmoins, mon approche est avant tout humaine**. 

Cela passe par la sensibilisation des employés, la revue des processus internes et externes, et la mise en place de plans de sécurité (laissez-moi vous accompagner pour comprendre ces PRA, PCA, PSSI, et tous ces acronymes qui font un peu peur ... voir page : ["Conseil & Gouvernance"]({{< ref "/services/conseil" >}})).

Enfin, je suis persuadé que **l'accompagnement cybersécurité est une assurance qui doit être disponible pour toutes les bourses** ! [N'hésitez pas à me contacter pour en discuter, il y a une solution adaptée à chaque profil](mailto:contact@paraselene.fr).

## A savoir : je travaille en coopérative !

Pour la petite histoire, je suis entrepreneur salarié, porté par la [coopérative d'activité ESS'AIN](https://www.essain.fr/) (*curieux de ce montage ? [plus d'informations sur les CAE sur le site de BPI France](https://bpifrance-creation.fr/encyclopedie/differentes-facons-dentreprendre/entreprendre-autrement/cooperatives-dactivites)*).