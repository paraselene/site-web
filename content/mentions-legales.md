---
title: "Mentions légales"
type: "page"
slug: "mentions-legales"
---

## Coordonnées

### Directeur de publication

Adrien Lasserre
[contact@paraselene.fr](mailto:contact@paraselene.fr)
{{< param "phone" >}}


### Entreprise

Parasélène est une marque d'**ESSAIN - Coopérative d'Activités et d'Emploi**

Maison de la Culture et de la Citoyenneté
4 allée des Brotteaux
01000 BOURG EN BRESSE

N° SIRET : 83071871400025
N° TVA : FR47830718714

## Hébergement

Le site internet de Parasélène est hébergé par Framasoft (_service Framagit_) sur un serveur Hetzner :  Hetzner Online GmbH - Industriestr. 25 - 91710 Gunzenhausen, Allemagne. 

## Droit d'auteur et licences

Sauf mention contraire, le contenu de ce site internet est protégé par le droit d'auteur en loi française.

### Logo et charte graphique

Bertrand Paris-Romaskevich pour [Tadaa](https://tadaa.fr)

### Thème du site internet

Hugo Initio par [Miguel Simoni](https://github.com/miguelsimoni/hugo-initio) et [GetTemplate / Sergey Pozhilov](https://www.gettemplate.com/)

### Bibliothèque carousel Javascript

Un carousel très léger (20Ko tout mouillé non compressé), en licence MIT : [Swiffy Slider](https://github.com/dynamicweb/swiffy-slider)

### Carte de France (SVG)

[Comersis](https://comersis.com)

### Tout autre contenu

Adrien Lasserre | Parasélène


## Protection de la vie privée

Le site ne dépose aucun cookie de suivi sur votre navigateur. Il utilise le logiciel [Matomo](https://fr.matomo.org/) pour l'analyse des visites (anonymisées).