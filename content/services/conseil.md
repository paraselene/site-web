+++
title = "Conseil & gouvernance"
weight = 1
type = "service"
icon = "far fa-edit"
+++

Audit de votre Système d’Information existant et livraison de **recommandations techniques et organisationnelles**.

<!--more-->

## Audit de sécurité

- Analyse de votre Système d'Information (SI) et de vos processus internes
- Remise d'un **livrable détaillé** et de préconisations concrètes en vue de renforcer la sécurité de votre SI (**plan d'action détaillé**)
- Travail en partenariat avec vos prestataires existants et des experts du secteur au besoin


## Sensibilisation des équipes

Le facteur humain est l'un des principaux vecteurs de risques en cybersécurité. Renforcer la formation et la sensibilisation des salariés est donc primordial&nbsp;:

- ![QUALIOPI](/logo-qualiopi-72dpi.png) **Formations certifiées QUALIOPI**
- Mise en place de **plans de formation** et d'outils pour la **formation continue des équipes** sur des sujets tels que le phishing, la gestion des mots de passe ou l'onboarding/offboarding (*intégration et départ de collaborateurs*)

## Réalisation de PCA/PRA

- Aide à la rédaction, implémentation et tests de **Plans de Continuité d'Activité** et **Plans de Reprise d'Activité**
- Intégration de solutions de sauvegardes (online et offline)
- Mise en oeuvre de solutions de supervision de l'état du système


## Labellisation

**Certifié ISO27001 Lead Implementer, 27005 et EbiosRM**, Parasélène peut vous accompagner dans la mise en oeuvre d'un SMSI (*système de management de la sécurité de l'information*) certifié ISO27001.
