+++
title = "Expertise en développement"
weight = 3
icon = "fab fa-connectdevelop"
type = "service"
+++

Parasélène accompagne vos équipes de développeurs pour **construire et structurer des applicatifs web sécurisés**.

<!--more-->

## Revue de code

- Expertise sur les gestionnaires de contenus les plus courants (CMS Drupal, Wordpress)
- Audit et préconisation de code métier existant et des pratiques de développement utilisées

## Architecture logicielle

- Aide à l'intégration de solutions de SSO ("Single Sign On") et MFA/2FA (authentification multifacteur) dans vos applications métiers
- Intégration des tests de sécurité dans vos processus d'intégration continu

## Devops et architecture système

- Sécurisation de solutions Openstack, Kubernetes / Openshift
- Audit et sécurisation de serveurs webs
